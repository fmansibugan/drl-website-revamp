var gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	minify = require('gulp-minify');

// Custom Styling
gulp.task('style', () => {
	return gulp.src([
			'src/scss/global.scss',
			'src/scss/*.scss'
		])
		.pipe(concat('style.css'))
		.pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
		.pipe(minify())
		.pipe(gulp.dest('./css/'))
});

// Main JS
gulp.task('scripts', () => {
	return gulp.src('src/main.js')
	.pipe(concat('main.js'))
	.pipe(minify({
		preserveComments: 'some'
	}))
	.pipe(gulp.dest('./js/'));
});

// Watch task
gulp.task('watch', () => {
  	gulp.watch('src/scss/*.scss', gulp.series('style'));
  	gulp.watch('src/main.js', gulp.series('scripts'));
});

// Build task
gulp.task('default', gulp.series('style','scripts','watch'));