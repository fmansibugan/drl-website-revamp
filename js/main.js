/*!
 * Author: Dynamic Range Labs, 2021
 * Website: wwww.dynamicrangelabs.com
*/

$(function(){ 

   // Matching logic
   var desktopMatch = !0, mobileMatch = !0;

   function matching() {
      desktopMatch = 1367 <= $(window).width(), 
      mobileMatch = $(window).width() <= 1366;
   }

   // Resets
   function resets() {

      if(1 == desktopMatch) {
         $('.main-menu').removeClass('open-panel');
      }

   }

   // Load
   $(window).on('resize',function() {
      matching(); // Matching logic
      resets(); // Resets
   });

   // Begin Others
	
   $('.menu-bar').click('on', function(){
      $('.main-menu').toggleClass('open-panel');
   });
   $('.close-menu').click('on', function(){
      $('.main-menu').removeClass('open-panel');
   });
});